FROM python:3.8.11-alpine3.14
COPY requirements.txt /opt/app/requirements.txt
# RUN apt-get update
# RUN apt-get install -y python3 python3-pip
WORKDIR /opt/app
RUN pip3 install -r requirements.txt
COPY dagda /opt/app
EXPOSE 5005
VOLUME [ "/var/run/docker.sock:/var/run/docker.sock:ro" ]
VOLUME [ "/tmp:/tmp" ]
ENTRYPOINT [ "python3", "dagda.py" ]